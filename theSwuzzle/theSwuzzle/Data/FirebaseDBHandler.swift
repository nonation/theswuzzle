//
//  FirebaseDBHandler.swift
//  theSwuzzle
//
//  Created by Henric Hiljanen on 2018-11-28.
//  Copyright © 2018 HenricHiljanen. All rights reserved.
//

import Foundation
import FirebaseDatabase

class FirebaseDBHandler {
    
    let TOP5_KEY: String = "top5"
    static let _shared = FirebaseDBHandler()
    let _ref = Database.database().reference()
    
    init () {}
    
}

extension FirebaseDBHandler {
    //MARK: - fetch all five children from DB
    func getTop5Players(completion: ((Bool, [Top5Player]) -> Void)?) {
        
        _ref.child(TOP5_KEY).observeSingleEvent(of: .value, with: {(snapshot) in
            var fetchedPlayers: [Top5Player] = []
            if snapshot.hasChildren() {
                let children = snapshot.children
                while let snap = children.nextObject() as? DataSnapshot, let dict = snap.value {
                    if let playerDict = dict as? [String: Any] {
                        let fetchedPlayer = Top5Player(dict: playerDict)
                        fetchedPlayers.append(fetchedPlayer)
                    }
                }
            }
            completion!(true, fetchedPlayers)
        }) { (error) in
            print(error.localizedDescription)
            completion!(false, [])
        }
    }
    
    //MARK: - Update Top5Player array
    func updateTop5Players (player: Top5Player, top5Players: [Top5Player], completion: ((Bool) -> Void)?) {
        let tempNr1: Top5Player = top5Players[0].copy()
        let tempNr2: Top5Player = top5Players[1].copy()
        let tempNr3: Top5Player = top5Players[2].copy()
        let tempNr4: Top5Player = top5Players[3].copy()
        
        // Spelaren längst ned försvinner oavsett när en ny spelare hamnar på highscoren
        //let tempNr5: Top5Player = top5Players[4].copy()
        
        var newTop5Players: [Top5Player] = []
        
        switch player.getPlace() {
        case 1:
            newTop5Players = [player, tempNr1, tempNr2, tempNr3, tempNr4]
        case 2:
            newTop5Players = [tempNr1, player, tempNr2, tempNr3, tempNr4]
        case 3:
            newTop5Players = [tempNr1, tempNr2, player, tempNr3, tempNr4]
        case 4:
            newTop5Players = [tempNr1, tempNr2, tempNr3, player, tempNr4]
        case 5:
            newTop5Players = [tempNr1, tempNr2, tempNr3, tempNr4, player]
        default:
            print("An invalid place in highscore was tried to be updated.")
        }
        
        updateFirebase(newtop5Players: newTop5Players)
    }
    
    //MARK: - Updates Firebase Database
    func updateFirebase (newtop5Players: [Top5Player]) {
        
        let nr1: [String: Any] = convertPlayerToFirebaseStructure(player: newtop5Players[0])
        let nr2: [String: Any] = convertPlayerToFirebaseStructure(player: newtop5Players[1])
        let nr3: [String: Any] = convertPlayerToFirebaseStructure(player: newtop5Players[2])
        let nr4: [String: Any] = convertPlayerToFirebaseStructure(player: newtop5Players[3])
        let nr5: [String: Any] = convertPlayerToFirebaseStructure(player: newtop5Players[4])
        
        _ref.child(TOP5_KEY).child("1").setValue(nr1)
        _ref.child(TOP5_KEY).child("2").setValue(nr2)
        _ref.child(TOP5_KEY).child("3").setValue(nr3)
        _ref.child(TOP5_KEY).child("4").setValue(nr4)
        _ref.child(TOP5_KEY).child("5").setValue(nr5)
    }
    
    func convertPlayerToFirebaseStructure (player: Top5Player) -> [String: Any]{
        
        let firebasePlayer: [String: Any] = ["name": player.getName(),
                                             "swaps": player.getSwaps(),
                                             "minutes": player.getTime().getMinutes(),
                                             "seconds": player.getTime().getSeconds(),
                                             "fraction": player.getTime().getFraction()]
        return firebasePlayer
    }
    
}

