//
//  HighscoreVC.swift
//  Swazzle
//
//  Created by Henric Hiljanen on 2018-11-12.
//  Copyright © 2018 NoNation. All rights reserved.
//

import UIKit

class HighscoreVC: UIViewController {

    @IBOutlet weak var dismissButton: BorderedButton!
    @IBOutlet weak var highscoreTableView: UITableView!
    
    var _top5Players: [Top5Player] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        highscoreTableView.delegate = self
        highscoreTableView.dataSource = self
        highscoreTableView.separatorStyle = .none
        dismissButton.layer.cornerRadius = self.view.frame.size.height * 0.3 * 0.5 / 2
        
        fetchData()
    }

    @IBAction func dismissHighscoreVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension HighscoreVC {
    //MARK: - Fetch Data from DB
    func fetchData() {
        FirebaseDBHandler._shared.getTop5Players { (success, top5Players) in
            DispatchQueue.main.async {
                if success {
                    self._top5Players = top5Players
                    self.highscoreTableView.reloadData()
                } else {
                    print("Couldn't fetch top5Players")
                }
            }
        }
    }
}

extension HighscoreVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _top5Players.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as? HighscoreCell {
            let top5Player = _top5Players[indexPath.row]
            cell.setCellLabels(player: top5Player)
            cell.setCellImage(index: indexPath.row)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
}
