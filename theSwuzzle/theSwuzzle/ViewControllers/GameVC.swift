//
//  GameVC.swift
//  Swazzle
//
//  Created by Henric Hiljanen on 2018-11-08.
//  Copyright © 2018 NoNation. All rights reserved.
//


import UIKit
import AudioToolbox
import FirebaseDatabase
import UserNotifications

class GameVC: UIViewController {
    
    let generator = UIImpactFeedbackGenerator(style: .heavy) //liten vibration (feedback)
    var _countSwaps = 0 //antal swaps spelaren har gjort (spara ner i highscores)
    var timer = Timer() //timern
    var startTime = TimeInterval()
    var playing = 0 // för att sätta på timern
    var _place = 0
    var _top5Players: [Top5Player] = []
    
    @IBOutlet weak var nOfSwaps: UILabel! //label för antal swaps
    @IBOutlet weak var gameTimer: UILabel! //label för tiden
    var _fraction: Int = 0
    var _seconds: Int = 0
    var _minutes: Int = 0
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var playAgainButton: BorderedButton!
    @IBOutlet weak var dismissButton: BorderedButton!
    @IBOutlet weak var statsFrameView: UIView!
    @IBOutlet weak var resetButton: BorderedButton!
    
    @IBAction func dismissGameVC (_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func resetButtonPressed (_ sender: Any) {
        stopTimer()
        resetGame()
    }
    
    @IBAction func playAgain (_ sender: Any) {
        generator.impactOccurred()
        playAgainButton.isHidden = true
        resetButton.isHidden = false
        resetGame()
        fetchData()
    }
    
    @IBAction func didUnwindFromTop5ModalVC (_ sender: UIStoryboardSegue) {
    }
    
    // Uppdaterar tiden i spelet
    @objc func updateTime () {
        
        let currentTime = NSDate.timeIntervalSinceReferenceDate
        
        var elaspedTime: TimeInterval = currentTime - startTime
        
        _minutes = Int(elaspedTime / 60.0)
        
        elaspedTime -= (TimeInterval(_minutes)*60)
        
        _seconds = Int(elaspedTime)
        
        elaspedTime -= TimeInterval(_seconds)
        
        _fraction = Int(elaspedTime * 100)
        
        let strMinutes = String(format: "%02d", UInt8(_minutes))
        let strSeconds = String(format: "%02d", UInt8(_seconds))
        let strFraction = String(format: "%02d", UInt8(_fraction))
        
        gameTimer.text = "\(strMinutes):" + "\(strSeconds):" + "\(strFraction)"  //printar ut tiden på laben med i formatet 00:00:00 min/sec/millisec
    }
    
    var _firstIndexPath: IndexPath?
    var _secondIndexPath: IndexPath?
    
    var gamePictures: [gamePicObj] = [
        gamePicObj(imageName: "0", isSelected: false),
        gamePicObj(imageName: "1", isSelected: false),
        gamePicObj(imageName: "2", isSelected: false),
        gamePicObj(imageName: "3", isSelected: false),
        gamePicObj(imageName: "4", isSelected: false),
        gamePicObj(imageName: "5", isSelected: false),
        gamePicObj(imageName: "6", isSelected: false),
        gamePicObj(imageName: "7", isSelected: false),
        gamePicObj(imageName: "8", isSelected: false)]
    
    let correctAns = ["0", "1", "2", "3", "4", "5", "6", "7", "8"]
    
    
    override func viewDidLoad () {
        super.viewDidLoad()
        
        askAboutNotifications() // fråga om notifications
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        gamePictures.shuffle()
        
        playAgainButton.layer.cornerRadius = self.view.frame.size.width * 0.3 / 2
        playAgainButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        resetButton.layer.cornerRadius = self.view.frame.size.width * 0.2 / 2
        resetButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        dismissButton.layer.cornerRadius = resetButton.layer.cornerRadius * 0.75
        
        nOfSwaps.text = String(_countSwaps) //sätter swaps till 0 när man kommer in på sidan
        
        fetchData()
        
        push() //skapa notification
        
    }
    
}

//MARK: - Logics
extension GameVC {
    // MARK: - FetchData from DB
    func fetchData() {
        FirebaseDBHandler._shared.getTop5Players { (success, top5Players) in
            DispatchQueue.main.async {
                if success {
                    self._top5Players = top5Players
                } else {
                    print("Couldn't fetch top5Players")
                }
            }
        }
    }
    
    func resetGame () {
        _countSwaps = 0
        nOfSwaps.text = String(_countSwaps)
        gameTimer.text = "00:00:00"
        playing = 0
        
        let cells = collectionView.visibleCells
        for cell in cells {
            cell.layer.borderWidth = 0
            cell.isSelected = false
        }
        
        _firstIndexPath = nil
        _secondIndexPath = nil
        
        collectionView.reloadData()
        
        gamePictures.shuffle()
    }
    
    // När man klarat pusslet
    func compareWithHighscore () {
        stopTimer()
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate)) //vanlig vibration
        
        // Skapar ett Time-objekt med hjälp av tiden som har gått
        let myTime = Time(minutes: _minutes, seconds: _seconds, fraction: _fraction)
        
        // Jämför med highscore för att se vilken plats man hamnat på
        _place = evaluate(swaps: _countSwaps, time: myTime)
        
        // Väljer vilken popup ska ska visas beroende på vilken placering man fått
        if _place == 0 {
            performSegue(withIdentifier: "underTop5Segue", sender: self)
        } else {
            performSegue(withIdentifier: "top5Segue", sender: self)
        }
        
        resetButton.isHidden = true
        playAgainButton.isHidden = false
    }
    
    func evaluate (swaps: Int, time: Time) -> Int {
        if (_top5Players[0].getSwaps() > swaps || (_top5Players[0].getSwaps() == swaps && _top5Players[0].getTime() > time)) {
            return 1
        }
        else if (_top5Players[1].getSwaps() > swaps || (_top5Players[1].getSwaps() == swaps && _top5Players[1].getTime() > time)) {
            return 2
        }
        else if (_top5Players[2].getSwaps() > swaps || (_top5Players[2].getSwaps() == swaps && _top5Players[2].getTime() > time)) {
            return 3
        }
        else if (_top5Players[3].getSwaps() > swaps || (_top5Players[3].getSwaps() == swaps && _top5Players[3].getTime() > time)) {
            return 4
        }
        else if (_top5Players[4].getSwaps() > swaps || (_top5Players[4].getSwaps() == swaps && _top5Players[4].getTime() > time)) {
            return 5
        }
        else {
            return 0
        }
    }
    
    // Sätter på timern när första swapet görs
    func isPLaying () {
        if playing == 1 {
            reset()
        }
    }
    
    override func prepare (for segue: UIStoryboardSegue, sender: Any?) {
        
        if _place != 0 {
            let destinationVC = segue.destination as! Top5ModalVC
            let time = Time(minutes: _minutes, seconds: _seconds, fraction: _fraction)
            destinationVC._place = _place
            destinationVC._time = time
            destinationVC._swaps = _countSwaps
            destinationVC._top5Players = _top5Players
        }
    }
    
    // Räknar swaps
    func updateSwaps () {
        _countSwaps += 1
        nOfSwaps.text = String(_countSwaps) //uppdaterar antalet swaps
    }
    
    // Startar tiden
    func startTimer () {
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
        startTime = NSDate.timeIntervalSinceReferenceDate
    }
    
    // Stoppar tiden
    func stopTimer () {
        timer.invalidate()
    }
    
    func reset () {
        timer.invalidate()
        startTimer()
    }
    
    func push(){ //skapar en push notificaiton som kommer upp en gång per 24h
        
        let content = UNMutableNotificationContent()
        content.body = "Long time no see, time for game?"
        content.badge = 1 //lägger till en röd badge uppe i hörnet. Den tas bort varje gång appen startar (finns i appDelegate)
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false) //popup en gång om dagen (86 000 sec)
        let request = UNNotificationRequest(identifier: "push", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    func askAboutNotifications() { //fråga om notificaitons
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
        }
    }
}

// MARK: UICollectionView
extension GameVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ModalDelegate {
    
    // Bestämmer hur många celler CollectionView:n ska ha
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gamePictures.count
    }
    
    // Swap-funktionen
    func btnSwapAction () {
        guard let start = self._firstIndexPath, let end = self._secondIndexPath else { return }
        
        self.collectionView.performBatchUpdates({ () -> Void in
            self.collectionView.moveItem(at: start, to: end);
            self.collectionView.moveItem(at: end, to: start);
        },completion:nil );

        self.gamePictures.swapAt(start.item, end.item)
        
        self._firstIndexPath = nil
        self._secondIndexPath = nil
        
        // Tar bort markeringarna runt de valda pusselbitarna
        let cell = self.collectionView.cellForItem(at: start)
        let cell2 = self.collectionView.cellForItem(at: end)
        cell?.layer.borderWidth = 0
        cell2?.layer.borderWidth = 0
        
        self.generator.impactOccurred() //feedback på varje swap
        self.updateSwaps() // räkna varje swap
        
        if self.playing == 0 { //om timern inte är igång så sätter den på den och kör igång timern
            self.playing = 1
            self.isPLaying()
        }
        
        if self.puzzleComplete() {
            self.compareWithHighscore()
        }
        
    }
    
    func puzzleComplete() -> Bool {
        // Bildens namn ska stämma överens med vilket index dess cell har i CollectionView:n
        for i in Array(0..<self.gamePictures.count) {
            if String(i) != self.gamePictures[i].imageName {
                return false
            }
        }
        return true
    }

    // Fyller CollectionView:ns celler med innehåll
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CVCell", for: indexPath) as! CVCell
        let gamePic: String = self.gamePictures[indexPath.row].imageName
        cell.imageView.image = UIImage(named: gamePic)

        return cell
        
    }
    
    // Körs när man tryckt på en pusselbit
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.gamePictures[indexPath.item].isSelected = true
        
        let cell = collectionView.cellForItem(at: indexPath)
        
        cell?.layer.borderColor = UIColor.orange.cgColor
        cell?.layer.borderWidth = 3
        
        // Gör så vi har koll på vilka två index de två valda cellerna har
        // i CollectionView:nsom vi vill swapa
        if self._firstIndexPath == nil{
            self._firstIndexPath = indexPath
        } else if self._firstIndexPath != nil {
            self._secondIndexPath = indexPath
            self.btnSwapAction()
        }
   
    }
    
    // Körs när pusselbitarna swap:at plats
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        self.gamePictures[indexPath.item].isSelected = false
        let cell = collectionView.cellForItem(at: indexPath)
        
        cell?.layer.borderWidth = 0
        
    }

    // Bestämmer cellernas storlek
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.view.frame.size.width/3
        return CGSize(width: width, height: width)
    }
    
    func dataUpdated () {
        self.fetchData()
    }
}
