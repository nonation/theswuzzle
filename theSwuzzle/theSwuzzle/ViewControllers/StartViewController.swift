//
//  ViewController.swift
//  Swuzzle
//
//  Created by Henric Hiljanen on 2018-10-31.
//  Copyright © 2018 NoNation. All rights reserved.
//

import UIKit

class StartViewController: UIViewController, UIViewControllerTransitioningDelegate  {
    
    let transition = CircularTransition()
    var transitionCircleColor: UIColor = UIColor.clear

    @IBOutlet weak var startGameButton: BorderedButton!
    @IBOutlet weak var hiwButton: BorderedButton!
    @IBOutlet weak var highscoreButton: BorderedButton!
    
    var startingPoint: CGPoint = CGPoint(x: 0.0, y: 0.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startGameButton.layer.cornerRadius = view.frame.size.height * 0.3 / 2
        
        hiwButton.layer.cornerRadius = startGameButton.layer.cornerRadius * 0.5
        hiwButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        highscoreButton.layer.cornerRadius = hiwButton.layer.cornerRadius
        highscoreButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
    }
    
    @IBAction func swuzzleButtonPressed(_ sender: Any) {
        UIView.animate(withDuration: 0.5) {
            self.startGameButton.backgroundColor = UIColor(red: 0.968, green: 0.780, blue: 0.248, alpha: 0.92)
        }

        UIView.animate(withDuration: 0.5) {
            self.startGameButton.backgroundColor = UIColor(red: 1, green: 0.851, blue: 0.278, alpha: 1)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "toGame") {
            let gameVC = segue.destination as! GameVC
            gameVC.transitioningDelegate = self
            gameVC.modalPresentationStyle = .custom
            startingPoint = startGameButton.center
            transitionCircleColor = startGameButton.backgroundColor!
        }
        else if (segue.identifier == "toHighscore") {
            let highscoreVC = segue.destination as! HighscoreVC
            highscoreVC.transitioningDelegate = self
            highscoreVC.modalPresentationStyle = .custom
            startingPoint = highscoreButton.center
            transitionCircleColor = highscoreButton.backgroundColor!
        }
        else if (segue.identifier == "toHIW") {
            let hiwVC = segue.destination as! HIWViewController
            hiwVC.transitioningDelegate = self
            hiwVC.modalPresentationStyle = .custom
            startingPoint = hiwButton.center
            transitionCircleColor = hiwButton.backgroundColor!
        }
        
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .present
        transition.startingPoint = startingPoint
        transition.circleColor = transitionCircleColor
        
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .dismiss
        transition.startingPoint = startingPoint
        transition.circleColor = transitionCircleColor
        
        return transition
    }
    
}

