//
//  Top5ModalVC.swift
//  Swazzle
//
//  Created by Henric Hiljanen on 2018-11-12.
//  Copyright © 2018 NoNation. All rights reserved.
//

import UIKit

class Top5ModalVC: UIViewController {
    
    var delegate: ModalDelegate?
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var modalView: UIView!
    @IBOutlet weak var saveButton: BorderedButton!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    
    var nameInput = String()
    var _place = 0
    var _time = Time()
    var _swaps = 0
    var _top5Players: [Top5Player] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        modalView.layer.cornerRadius = backgroundView.frame.size.width * 0.7 / 2
        modalView.layer.masksToBounds = true
        
        adaptDesign()
    }
    
    // Bestämmer vilken bild som ska visas
    func adaptDesign() {
        if _place == 1 {
            imageView.image = UIImage(named: "gold")
        }
        else if _place == 2 {
            imageView.image = UIImage(named: "silver")
        }
        else if _place == 3 {
            imageView.image = UIImage(named: "bronze")
        }
        else if _place == 4 {
            imageView.image = UIImage(named: "fourth")
        }
        else if _place == 5 {
            imageView.image = UIImage(named: "fifth")
        }
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        var name = textField.text!
        
        if name == "" {
            name = "(UNKNOWN)"
        }
        
        let newTop5Player = Top5Player(name: name, swaps: _swaps, time: _time, place: _place)
        
        FirebaseDBHandler._shared.updateTop5Players(player: newTop5Player, top5Players: _top5Players) { (success) in
            if success {
                if let delegate = self.delegate {
                    delegate.dataUpdated()
                }
                self.dissmissVC()
            } else {
                print("Couldn't update")
            }
        }
    }
    
    // Återgår till GameVC
    @objc func dissmissVC() {
        dismiss(animated: true, completion: nil)
    }

}
