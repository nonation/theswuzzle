//
//  HIWViewController.swift
//  Swazzle
//
//  Created by Henric Hiljanen on 2018-11-12.
//  Copyright © 2018 NoNation. All rights reserved.
//

import UIKit

class HIWViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var dismissButton: BorderedButton!
    
    var images: [String] = ["HIW1", "HIW2", "HIW3", "HIW4"]
    var frame: CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
    
    @IBAction func dismissHIWVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dismissButton.layer.cornerRadius = self.view.frame.size.height * 0.3 * 0.5 / 2
        
        pageControl.numberOfPages = images.count
        
        scrollView.contentSize = CGSize(width: self.view.bounds.width * CGFloat(images.count), height: scrollView.frame.size.height)
        
        for index in 0..<images.count {
            frame.origin.x = scrollView.frame.size.width * CGFloat(index)
            frame.size = scrollView.frame.size
            
            let imageView = UIImageView(frame: frame)
            imageView.contentMode = .scaleAspectFit
            imageView.image = UIImage(named: images[index])
            
            self.scrollView.addSubview(imageView)
            imageView.frame.size.width = self.view.bounds.size.width
            imageView.frame.size.height = scrollView.frame.size.height
            imageView.frame.origin.x = CGFloat(index) * self.view.bounds.size.width
        }
        
        scrollView.delegate = self
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = scrollView.contentOffset.x / scrollView.frame.size.width
        pageControl.currentPage = Int(pageNumber)
        
    }

}
