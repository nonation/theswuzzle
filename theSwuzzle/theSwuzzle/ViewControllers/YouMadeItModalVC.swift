//
//  YouMadeItModalVC.swift
//  Swazzle
//
//  Created by Henric Hiljanen on 2018-11-12.
//  Copyright © 2018 NoNation. All rights reserved.
//

import UIKit

class YouMadeItModalVC: UIViewController {

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var modalView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Trycker man utanför så kommer man tillbaka till GameVC
        let tap = UITapGestureRecognizer(target: self, action: #selector(dissmissVC))
        backgroundView.addGestureRecognizer(tap)
        
        modalView.layer.cornerRadius = backgroundView.frame.size.width * 0.7 / 2
        
    }
    
    @objc func dissmissVC() {
        dismiss(animated: true, completion: nil)
    }

}
