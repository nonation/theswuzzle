//
//  Time.swift
//  theSwuzzle
//
//  Created by Henric Hiljanen on 2018-11-28.
//  Copyright © 2018 HenricHiljanen. All rights reserved.
//

import Foundation
import UIKit

class Time {
    private var _minutes: Int
    private var _seconds: Int
    private var _fraction: Int
    
    init (minutes: Int = 59, seconds: Int = 59, fraction: Int = 99) {
        self._minutes = minutes
        self._seconds = seconds
        self._fraction = fraction
    }
    
    func getMinutes () -> Int {
        return self._minutes
    }
    
    func getSeconds () -> Int {
        return self._seconds
    }
    
    func getFraction () -> Int {
        return self._fraction
    }
    
    func setNewTime (minutes: Int, seconds: Int, fraction: Int) {
        _minutes = minutes
        _seconds = seconds
        _fraction = fraction
    }
    
    static func < (lhs: Time, rhs: Time) -> Bool {
        let ltFractionSum = lhs.getMinutes() * 60 * 100 + lhs.getSeconds() * 100 + lhs.getFraction()
        let rtFractionSum = rhs.getMinutes() * 60 * 100 + rhs.getSeconds() * 100 + rhs.getFraction()
        if ltFractionSum < rtFractionSum {
            return true
        } else {
            return false
        }
        
    }
    
    static func > (lhs: Time, rhs: Time) -> Bool {
        let ltFractionSum = lhs.getMinutes() * 60 * 100 + lhs.getSeconds() * 100 + lhs.getFraction()
        let rtFractionSum = rhs.getMinutes() * 60 * 100 + rhs.getSeconds() * 100 + rhs.getFraction()
        if ltFractionSum > rtFractionSum {
            return true
        } else {
            return false
        }
        
    }
}
