//
//  Top5Player.swift
//  theSwuzzle
//
//  Created by Henric Hiljanen on 2018-11-28.
//  Copyright © 2018 HenricHiljanen. All rights reserved.
//

import Foundation
import UIKit

class Top5Player : NSObject, NSCopying {
    
    private var _name: String
    private var _time: Time
    private var _swaps: Int
    private var _place: Int
    
    init (name: String, swaps: Int, time: Time, place: Int) {
        self._name = name
        self._swaps = swaps
        self._time = time
        self._place = place
    }
    
    override init () {
        self._name = ""
        self._swaps = 999
        self._time = Time()
        self._place = 0
    }
    
    init (dict: [String: Any]) {
        self._name = dict["name"] as? String ?? ""
        self._swaps = dict["swaps"] as? Int ?? 99
        self._time = Time(minutes: dict["minutes"] as? Int ?? 59, seconds: dict["seconds"] as? Int ?? 59, fraction: dict["fraction"] as? Int ?? 99)
        self._place = 0
    }
    
    func getName () -> String {
        return self._name
    }
    
    func getSwaps () -> Int {
        return self._swaps
    }
    
    func getTime () -> Time {
        return self._time
    }
    
    func getPlace () -> Int {
        return self._place
    }
    
    func setName (name: String) {
        self._name = name
    }
    
    func setSwaps (swaps: Int) {
        self._swaps = swaps
    }
    
    func setTime (time: Time) {
        self._time = time
    }
    
    func setPlace (place: Int) {
        self._place = place
    }
    
    func copy () -> Top5Player {
        return Top5Player(name: _name, swaps: _swaps, time: _time, place: _place)
    }

    func copy (with zone: NSZone? = nil) -> Any {
        return Top5Player(name: _name, swaps: _swaps, time: _time, place: _place)
    }
}
