//
//  borderedButton.swift
//  Swazzle
//
//  Created by Henric Hiljanen on 2018-11-12.
//  Copyright © 2018 NoNation. All rights reserved.
//

import UIKit

@IBDesignable class BorderedButton: UIButton {
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
