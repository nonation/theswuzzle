//
//  gamePicObj.swift
//  SwuzzleThePuzzleGame
//
//  Created by Marcus Presa Käld on 2018-11-13.
//  Copyright © 2018 Henric HIljanen. All rights reserved.
//

import Foundation

class gamePicObj {
    var imageName: String
    var isSelected: Bool
    
    init(imageName: String = "", isSelected: Bool = false) {
        self.imageName = imageName
        self.isSelected = isSelected
    }
}
