//
//  HighscoreCell.swift
//  Swuzzle
//
//  Created by Henric Hiljanen on 2018-10-31.
//  Copyright © 2018 NoNation. All rights reserved.
//

import UIKit

class HighscoreCell: UITableViewCell {

    @IBOutlet weak var HighscoreImageView: UIImageView!
    @IBOutlet weak var HighscoreNameLabel: UILabel!
    @IBOutlet weak var swapsLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    func setCellLabels(player: Top5Player) {
        HighscoreNameLabel.text = player.getName()
        swapsLabel.text = "Swaps: \(player.getSwaps())"
        
        let strMinutes = String(format: "%02d", UInt8(player.getTime().getMinutes()))
        let strSeconds = String(format: "%02d", UInt8(player.getTime().getSeconds()))
        let strFraction = String(format: "%02d", UInt8(player.getTime().getFraction()))
        
        timeLabel.text = "Time: \(strMinutes):" + "\(strSeconds):" + "\(strFraction)"
    }
    
    func setCellImage (index: Int) {
        HighscoreImageView.image = UIImage(named: indexToImageName(index: index))
    }
    
    func indexToImageName (index: Int) -> String {
        
        switch index {
        case 0:
            return "gold"
        case 1:
            return "silver"
        case 2:
            return "bronze"
        case 3:
            return "fourth"
        case 4:
            return "fifth"
        default:
            return ""
        }
        
    }
}
