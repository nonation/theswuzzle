//
//  ModalDelegate.swift
//  theSwuzzle
//
//  Created by Henric Hiljanen on 2018-11-30.
//  Copyright © 2018 HenricHiljanen. All rights reserved.
//

import Foundation

protocol ModalDelegate {
    func dataUpdated ()
}
